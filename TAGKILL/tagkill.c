#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <signal.h>

int main(int argc, char **argv){
	char *tag = NULL;
	int pid = 0;
	char line_buffer[256]; //hold a line in the file
	char *buffPtr  = line_buffer;
	FILE *f = fopen("/proc/ptags", "r");
	if(argc != 2){
		printf("Kindly specify a valid label\n");
		exit(EXIT_FAILURE);
	}
	tag = argv[1];
	if(f){
		while(fgets(line_buffer, sizeof(line_buffer), f)){
			sscanf(line_buffer, "%4d", &pid);
			memcpy(buffPtr, line_buffer+5, sizeof(line_buffer+5)-1);
			if(!strncmp(tag, buffPtr, sizeof(*tag))){
				kill(pid, SIGKILL);
				printf("killed pid: %d\n", pid);
			}
			pid = 0;
		}
	fclose(f);
	}
	else{
		printf("Error: pseudo devicefile [/proc/ptags] does not exist!");
                exit(EXIT_FAILURE);
	}

	return 0;
}
