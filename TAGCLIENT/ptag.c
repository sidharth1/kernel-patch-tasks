#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/syscall.h>

#define PTAG_CALL 337

int main(int argc, char **argv){
	long retVal = 0;	//return value of system call
	char option;
	char *label = NULL;
	int pid = 0;
	int command = 1337;	//ADD -> 1 && REMOVE -> 0
	pid = (!argv[1]) ? 0:atoi(argv[1]);
	if(!pid){
		printf("ERROR %d: pid cannot be null\n", -EXIT_FAILURE);
		printf("USAGE: ./ptag <process_id> <-a|-r> <\"tagstring\">\n");
		exit(EXIT_FAILURE);
	}
	while((option = getopt(argc, argv, "a:r:")) != -1){
		switch(option){
			case('a'): command = 1;
			       	label = (char *)optarg;
				break;
			case('r'): command = 0;
				label = (char *)optarg;
				break;
			default: printf("ERROR: please specify a valid option\n");
				printf("USAGE: ./ptag <process_id> <-a|-r> <\"tagstring\">\n");
				exit(EXIT_FAILURE);
		}
	}
	if(!label || argc != 4 || (command != 0 && command != 1)){
		printf("ERROR %d\n", -EXIT_FAILURE);
		printf("USAGE: ./ptag <process_id> <-a|-r> <\"tagstring\">\n");
		exit(EXIT_FAILURE);
	}
	
	retVal = syscall(PTAG_CALL, pid, NULL, label, command);
	if(retVal){
		printf("ERROR %d:  Invalid pid OR credentials. CHECK LOGS FOR DETAILS.\n", -EXIT_FAILURE);
		printf("USAGE: ./ptag <process_id> <-a|-r> <\"tagstring\">\n");
		exit(EXIT_FAILURE);
	}
	return 0;
}
