#include <stdio.h>
#include <stdlib.h>

#define BUFF_SIZE 256

int main(void){
	char line_buffer[BUFF_SIZE];
	FILE *f = fopen("/proc/ptags", "r");

	if(f){
		while(fgets(line_buffer, sizeof(line_buffer), f)){
			fputs(line_buffer, stdout);
		}
	fclose(f);
	}
	else{
		perror("Error: pseudo devicefile [/proc/ptags] does not exist!");
		exit(EXIT_FAILURE);
	}	
	return 0;
}
